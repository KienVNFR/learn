from re import S
from turtle import st
import cv2 
import numpy as np 


img=cv2.imread("frame_000003.PNG")
cor=open("gt_frame_000000.txt","r")
stack=cor.read()
cor=stack.splitlines()
tmp = [round(float(i)) for i in cor[1].split(',')]
result = list(zip(tmp[::2], tmp[1::2]))
mask=np.zeros(img.shape[:2],dtype="uint8")
isClosed=True
cv2.polylines(mask,(1188, 486), (1179, 483), (1171, 437), (1186, 421), (1180, 404), (1181, 393), (1190, 384), (1201, 381), (1217, 391), (1221, 403), (1220, 426), (1239, 446), (1251, 466), (1235, 481), (1188, 486),isClosed,(255,0,0),5)

print(result[0],result[1])
masked = cv2.bitwise_and(img, img, mask=mask)
print(type(result))
cv2.imshow("Mask Applied to Image", masked)
cv2.waitKey(0)
cv2.destroyAllWindows()