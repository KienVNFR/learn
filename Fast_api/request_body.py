from fastapi import FastAPI
from typing import Union
from pydantic import BaseModel

class Item(BaseModel):
	name:str 
	description:str
	money:float
	tax: Union[float,None] = None

app=FastAPI()

@app.get("/items/")
async def create_items(item:Item):
	return item
