from asyncio import QueueEmpty
from email.policy import default
from unittest import result
from unittest.util import _MAX_LENGTH
from fastapi import FastAPI,Query,Path
from typing import Union
app=FastAPI()
#Query parameter and string val
# @app.get("/items/")
# async def read_items(q:Union[str,None]= Query(default=None,min_length=3, max_length=50)): # default = ... or Required of ["",""] so we can query with union[list[str]]
# 	results={"items":[{"items":"foo"},{"itemd":"bar"}]}
# 	if q:
# 		return{"q":q}
# 	return results
#path para and numetric val
@app.get("/items/{item_id}")
#gt: greater than
# le: less than or equality

async def read_items(*,item_id: int = Path(title="The ID of the item to get",ge=1),q: Union[str, None] = Query(default=None, alias="item-query")):
	#size: float = Query(gt=0, lt=10.5)
	results = {"item_id": item_id}
	if q:
		results.update({"q": q})
	return results

