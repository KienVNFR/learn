from tkinter.messagebox import RETRY
from fastapi import FastAPI 
from typing import Union
app=FastAPI()

#try first step 
@app.get("/")
def root():
    return{"message":"hello world"}

#create path of prameter 
@app.get("/items/{items_id}")
def read_items(items_id):
    return{"items":items_id}

#set type of parameter 
@app.get("/itemstype/{items_int}")
def items_type(items:int):
    return{"itime_int":items}

# Query parameters 
fake_items_db=[{"name_id":"kien"},{"name_id1":"nam"},{"nameid2":"hai"}]
@app.get("/items/")
def query(skip: int=0,limit:int=10):
    return fake_items_db[skip:limit]

#option parameter if not string q be changed to none 
@app.get("/items_para/{item_id_para}")
def option(item_id:str,q:Union[str,None]=None):
    if q:
        return{"item_id":item_id,"q":q}
    return {"item_id":item_id}
    
#multil query parameter 
@app.get("/users/{user_id}/items/{item_id}")
async def read_user_item(
    user_id: int, item_id: str, add: Union[str, None] = None, short: bool = False
):
    item = {"item_id": item_id, "owner_id": user_id}
    if add:
        item.update({"q": add})
    if not short:
        item.update(
            {"description": "This is an amazing item that has a long description"}
        )
    return item

#change to  bool 
@app.get("/items_bool/{items_id_bool}")
def read_items(item_id:str,q:str,short:bool=False):
    item={"item_id":item_id}
    if q :
        item.update({"q":q})
    if not short:
        item.update({
            "description":"This is an amazing item that has a long description"
        })
    return item
    
# needy 
@app.get("/items_bool/{items_id_bool}")
def read_items(item_id:str, q: Union[str, None]=None , short:bool=False):
    item={"item_id":item_id}
    if q :
        item.update({"q":q})
    if not short:
        item.update({
            "description":"This is an amazing item that has a long description"
        })
    return item